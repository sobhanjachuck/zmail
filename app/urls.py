from app import app
from app.api import AddUserAPI, LoginAPI, LogoutAPI, ViewEmailsAPI, SaveAsDraftEmailAPI, SendEmailAPI, DeleteEmailAPI

add_user_view = AddUserAPI.as_view('add_user_api')
app.add_url_rule('/api/add_user/', view_func=add_user_view, methods=['POST'])

login_view = LoginAPI.as_view('login_api')
app.add_url_rule('/api/login/', view_func=login_view, methods=['POST'])

logout_view = LogoutAPI.as_view('logout_api')
app.add_url_rule('/api/logout/', view_func=logout_view, methods=['GET'])

view_emails_view = ViewEmailsAPI.as_view('view_emails_api')
app.add_url_rule('/api/emails/', view_func=view_emails_view, methods=['GET'])

save_draft_view = SaveAsDraftEmailAPI.as_view('save_draft_api')
app.add_url_rule('/api/email/save_as_draft/', view_func=save_draft_view, methods=['POST'])

send_email_view = SendEmailAPI.as_view('send_email_api')
app.add_url_rule('/api/email/send/', view_func=send_email_view, methods=['POST'])

delete_email_view = DeleteEmailAPI.as_view('delete_email_api')
app.add_url_rule('/api/email/delete/', view_func=delete_email_view, methods=['POST'])