
class UserAuthenticationException(Exception):
    def __init__(self):
        self.message = "Login Failed. Invalid credentials"


class InvalidRequestException(Exception):
    def __init__(self):
        self.message = "Failed to process request. Invalid input"


class UserLoggedOutException(Exception):
    def __init__(self):
        self.message = "User already logged out"
