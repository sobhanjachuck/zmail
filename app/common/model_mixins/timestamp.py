from app.db_init import db
import datetime


class TimeStampMixin(db.Model):
    __abstract__ = True

    created_on = db.Column(db.DateTime, default=datetime.datetime.now, index=True)
    updated_on = db.Column(db.DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)
