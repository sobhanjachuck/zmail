from flask import Flask
import os
import logging.config
#from conf.base import Config
#from flask_sqlalchemy import SQLAlchemy
#from flask_admin import Admin

ENV_CONF = {
    "local": "conf.local.LocalConfig",
}


####################### SETUP APP #####################################
def create_app():
    app = Flask(__name__)
    env_name = 'local'
    if env_name:
        app.config.from_object(ENV_CONF[env_name])
        logging.config.dictConfig(app.config['LOGGING_CONFIG'])

    logging.info({"method": "app.create_app",
                  "message": "Successfully Created App."})
    return app


app = create_app()

# Add all URLs
from app.api import welcome
from app.urls import *


######################## MIGRATION SETUP #############################
from flask_migrate import Migrate
from app.db_init import db


app_dir = os.path.dirname(os.path.realpath(__file__))
migrate = Migrate(app, db, directory=os.path.join(app_dir, 'migrations'))

# Import models for migration
from app.user.models.user import User
from app.email.models import Email, Inbox, Sent
