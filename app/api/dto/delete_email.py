from marshmallow import Schema, fields


class DeleteEmailDTO(Schema):
    email_id = fields.Integer(required=True, allow_none=False)
