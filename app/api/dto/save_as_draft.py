from marshmallow import Schema, fields


class SaveAsDraftDTO(Schema):
    email_id = fields.Integer(required=False, allow_none=True)
    subject = fields.String(required=True, allow_none=False)
    sender = fields.String(required=True, allow_none=False)
    receivers = fields.List(fields.String())
    body = fields.String(required=True, allow_none=False)
    in_reply_to_email = fields.Integer(required=False)
