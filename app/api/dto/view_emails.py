from marshmallow import Schema, fields


class ViewEmailsDTO(Schema):
    box_name = fields.String(required=True, allow_none=False)
    page_size = fields.Integer(required=True, allow_none=False, default=20)
    page_number = fields.Integer(required=True, allow_none=False, default=1)
