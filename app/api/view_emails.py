import json
import logging
from urllib.parse import unquote

from flask import Response, request
from flask.views import MethodView

from app.email.email_service import EmailService
from app.api.dto.view_emails import ViewEmailsDTO
from app.common.exceptions import InvalidRequestException, UserLoggedOutException


class ViewEmailsAPI(MethodView):
    def get(self):
        '''
        This REST endpoint is responsible for authenticating a user and logging him in

        :return: user details if user is valid.
        '''
        try:
            if not request.cookies.get('auth_user'):
                raise UserLoggedOutException()
            user_data = eval(unquote(request.cookies.get('auth_user')))
            username = user_data["username"]

            request_data = request.args

            view_emails_dto = ViewEmailsDTO().load(data=request_data)
            if view_emails_dto.errors:
                raise InvalidRequestException()

            result = EmailService.fetch_emails(
                username=username,
                box_name=request_data["box_name"],
                page_number=int(request_data["page_number"]),
                page_size=int(request_data["page_size"])
            )

            # Construct response
            json_data = {"success": True, "box_name": request_data["box_name"], "emails": result}
            logging.info(json_data)

            return Response(json.dumps(json_data), status=200, mimetype='application/json')
        except UserLoggedOutException as e:
            logging.error(e.message)
            return Response(json.dumps(dict(
                success=False,
                message=e.message)),
                status=401,
                mimetype='application/json')
        except InvalidRequestException as e:
            logging.error("{error} in request {request}".format(
                error=e.message,
                request=request_data
            ))
            return Response(json.dumps(dict(
                success=False,
                message=e.message)),
                status=400,
                mimetype='application/json')
        except Exception as e:
            logging.error({"message": e.__repr__()})
            return Response(json.dumps(dict(
                success=False,
                message="Something went wrong! {err}".format(err=e.__repr__()))),
                status=500,
                mimetype='application/json')

