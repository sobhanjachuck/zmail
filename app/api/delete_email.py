import json
import logging
from urllib.parse import unquote

from flask import Response, request
from flask.views import MethodView

from app.api.dto.delete_email import DeleteEmailDTO
from app.common.exceptions import InvalidRequestException, UserLoggedOutException
from app.email.email_service import EmailService


class DeleteEmailAPI(MethodView):
    def post(self):
        '''
        This REST endpoint is responsible for saving an email as Drafts

        :return: user details if user is valid.
        '''
        try:
            if not request.cookies.get('auth_user'):
                raise UserLoggedOutException()
            user_data = eval(unquote(request.cookies.get('auth_user')))
            username = user_data["username"]

            request_data = json.loads(request.data.decode('utf-8'))

            delete_email_dto = DeleteEmailDTO().load(data=request_data)
            if delete_email_dto.errors:
                raise InvalidRequestException()

            email_id = EmailService.delete_email(username, request_data["email_id"])

            # Construct response
            json_data = {"success": True, "email_id": email_id, "message": "Successfully deleted email"}
            logging.info(json_data)

            return Response(json.dumps(json_data), status=200, mimetype='application/json')
        except UserLoggedOutException as e:
            logging.error(e.message)
            return Response(json.dumps(dict(
                success=False,
                message=e.message)),
                status=401,
                mimetype='application/json')
        except InvalidRequestException as e:
            logging.error("{error} in request {request}".format(
                error=e.message,
                request=request_data
            ))
            return Response(json.dumps(dict(
                success=False,
                message=e.message)),
                status=400,
                mimetype='application/json')
        except Exception as e:
            logging.error({"message": e.__repr__()})
            return Response(json.dumps(dict(
                success=False,
                message="Something went wrong! {err}".format(err=e.__repr__()))),
                status=500,
                mimetype='application/json')

