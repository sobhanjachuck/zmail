import json
import logging

from flask import Response, request
from flask.views import MethodView

from app.common.exceptions import UserLoggedOutException


class LogoutAPI(MethodView):
    def get(self):
        '''
        This REST endpoint is responsible for authenticating a user and logging him in

        :return: user details if user is valid.
        '''
        try:
            user_data = request.cookies.get('auth_user')
            if not user_data:
                raise UserLoggedOutException()

            # Construct response
            json_data = {"success": True, "message": "Logout successful"}
            logging.info(json_data)

            response = Response(json.dumps(json_data), status=200, mimetype='application/json')
            response.delete_cookie(key="auth_user")
            return response
        except UserLoggedOutException as e:
            logging.error(e.message)
            return Response(json.dumps(dict(
                success=False,
                message="User already logged out")),
                status=400,
                mimetype='application/json')
        except Exception as e:
            logging.error({"message": e.__repr__()})
            return Response(json.dumps(dict(
                success=False,
                message="Something went wrong! {err}".format(err=e.__repr__()))),
                status=500,
                mimetype='application/json')

