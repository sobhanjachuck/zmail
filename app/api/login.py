import json
import logging
from urllib.parse import quote
from datetime import datetime, timedelta

from flask import Response, request
from flask.views import MethodView

from app import app
from app.api.dto.login import LoginDTO
from app.common.exceptions import UserAuthenticationException, InvalidRequestException
from app.user.user_service import UserService


class LoginAPI(MethodView):
    def post(self):
        '''
        This REST endpoint is responsible for authenticating a user and logging him in

        :return: user details if user is valid.
        '''
        try:
            request_data = json.loads(request.data.decode('utf-8'))

            login_dto = LoginDTO().load(data=request_data)
            if login_dto.errors:
                raise InvalidRequestException()

            result = UserService.authenticate_user(request_data["username"], request_data["password"])

            if not result:
                raise UserAuthenticationException()

            # Construct response
            json_data = {"success": True, "user": result, "message": "Login successful"}
            logging.info(json_data)

            response = Response(json.dumps(json_data), status=200, mimetype='application/json')
            response.set_cookie(
                key="auth_user",
                value=quote(json.dumps(result)),
                expires=datetime.utcnow() + timedelta(seconds=app.config["SESSION_COOKIE_AGE"])
            )
            return response
        except InvalidRequestException as e:
            logging.error("{error} in request {request}".format(
                error=e.message,
                request=request_data
            ))
            return Response(json.dumps(dict(
                success=False,
                message=e.message)),
                status=400,
                mimetype='application/json')
        except UserAuthenticationException as e:
            logging.error("{error} in request {request}".format(
                error=e.message,
                request=request_data
            ))
            return Response(json.dumps(dict(
                success=False,
                message=e.message)),
                status=401,
                mimetype='application/json')
        except Exception as e:
            logging.error({"message": e.__repr__()})
            return Response(json.dumps(dict(
                success=False,
                message="Something went wrong! {err}".format(err=e.__repr__()))),
                status=500,
                mimetype='application/json')

