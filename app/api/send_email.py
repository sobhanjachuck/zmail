import json
import logging
from urllib.parse import unquote

from flask import Response, request
from flask.views import MethodView

from app.api.dto.send_email import SendEmailDTO
from app.common.exceptions import InvalidRequestException, UserLoggedOutException
from app.email.email_service import EmailService


class SendEmailAPI(MethodView):
    def post(self):
        '''
        This REST endpoint is responsible for saving an email as Drafts

        :return: user details if user is valid.
        '''
        try:
            if not request.cookies.get('auth_user'):
                raise UserLoggedOutException()
            user_data = eval(unquote(request.cookies.get('auth_user')))
            username = user_data["username"]

            request_data = json.loads(request.data.decode('utf-8'))

            send_email_dto = SendEmailDTO().load(data=request_data)
            if send_email_dto.errors:
                raise InvalidRequestException()

            email_id = EmailService.send_email(username, send_email_dto.data)

            # Construct response
            json_data = {"success": True, "email_id": email_id, "message": "Successfully sent email"}
            logging.info(json_data)

            return Response(json.dumps(json_data), status=200, mimetype='application/json')
        except UserLoggedOutException as e:
            logging.error(e.message)
            return Response(json.dumps(dict(
                success=False,
                message=e.message)),
                status=401,
                mimetype='application/json')
        except InvalidRequestException as e:
            logging.error("{error} in request {request}".format(
                error=e.message,
                request=request_data
            ))
            return Response(json.dumps(dict(
                success=False,
                message=e.message)),
                status=400,
                mimetype='application/json')
        except Exception as e:
            logging.error({"message": e.__repr__()})
            return Response(json.dumps(dict(
                success=False,
                message="Something went wrong! {err}".format(err=e.__repr__()))),
                status=500,
                mimetype='application/json')

