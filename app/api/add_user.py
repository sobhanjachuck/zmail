import json
import logging
from flask import Response, request
from flask.views import MethodView
from app.user.user_service import UserService
from app.user.dto.user_dto import UserDTO


class AddUserAPI(MethodView):
    def post(self):
        '''
        This REST endpoint is responsible for adding a new user to the database

        :return: Success flag is true if user is successfully added to DB, Otherwise Succes flag is set to false
        '''
        try:
            request_data = json.loads(request.data.decode('utf-8'))

            new_user_dto = UserDTO().load(data=request_data)
            result = UserService.add_new_user(new_user_dto)

            # Construct response
            json_data = {"success": result["success"], "message": result.get("message")}
            logging.info(json_data)

            return Response(json.dumps(json_data), status=200, mimetype='application/json')

        except Exception as e:
            logging.error({"message": e.__repr__()})
            return Response(json.dumps(dict(
                success=False,
                message="Something went wrong! {err}".format(err=e.__repr__()))),
                status=500,
                mimetype='application/json')

