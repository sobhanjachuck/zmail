from app.common.model_mixins import TimeStampMixin
from app.db_init import db


class User(TimeStampMixin):
    __tablename__ = "user"
    __hash__ = object.__hash__

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True, nullable=False, index=True)
    password = db.Column(db.String(64), nullable=False)
    first_name = db.Column(db.String(64), nullable=False)
    last_name = db.Column(db.String(64), nullable=False)
    alt_email = db.Column(db.String(128))

    def __repr__(self):
        return '<User - {name} :: {username}>'.format(name=self.first_name, username=self.username)
