from marshmallow import (Schema, ValidationError, fields, validate,
                         validates_schema)


class UserDTO(Schema):
    username = fields.String(required=True, allow_none=False)
    password = fields.String(required=True, allow_none=False)
    first_name = fields.String(required=True, allow_none=False)
    last_name = fields.String(required=True, allow_none=False)
    alt_email = fields.String()
