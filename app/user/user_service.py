from app.user.models.user import User
from app.user.user_repository import UserRepository


class UserService:
    @classmethod
    def add_new_user(cls, new_user_dto):
        new_user_data = new_user_dto.data
        existing_user = UserRepository.get_user_by_username(new_user_data["username"])
        if existing_user:
            return {"success": False, "message": "User already exists"}
        UserRepository.add_user(new_user_data)
        return {"success": True, "message": "User registered successfully"}

    @classmethod
    def authenticate_user(cls, username, password):
        user = UserRepository.get_user_by_username(username)
        if user and user.password == password:
            return dict(
                username=user.username,
                first_name=user.first_name,
                last_name=user.last_name,
                alt_email=user.alt_email
            )
        return None
