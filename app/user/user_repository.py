from app.user.models.user import User


class UserRepository:
    @classmethod
    def add_user(cls, new_user_data):
        from app import db
        new_user = User()
        new_user.username = new_user_data["username"]
        new_user.password = new_user_data["password"]
        new_user.first_name = new_user_data["first_name"]
        new_user.last_name = new_user_data["last_name"]
        new_user.alt_email = new_user_data["alt_email"]
        db.session.add(new_user)
        db.session.commit()

    @classmethod
    def get_user_by_username(cls, username):
        return User.query.filter(User.username == username).first()
