from app.email.models import Email, Inbox, Sent
from app.common.constants import EMAIL_STATUS_INBOX, EMAIL_STATUS_DRAFT, EMAIL_STATUS_SENT, EMAIL_STATUS_THRASH
from app.common.exceptions import InvalidRequestException

class EmailRepository:
    @classmethod
    def fetch_inbox(cls, username, page_size, page_number):
        return Email.query.join(Inbox, Email.id == Inbox.email).add_columns(
            Email.id, Email.created_on, Email.subject, Email.sender, Email.receivers,
            Email.body, Email.in_reply_to_email, Inbox.read
        ).filter(Inbox.username == username).filter(Inbox.status == EMAIL_STATUS_INBOX).paginate(
            page_number, page_size, False
        )

    @classmethod
    def fetch_thrash(cls, username, page_size, page_number):
        return Email.query.join(Inbox, Email.id == Inbox.email).add_columns(
            Email.id, Email.created_on, Email.subject, Email.sender, Email.receivers,
            Email.body, Email.in_reply_to_email, Inbox.read
        ).filter(Inbox.username == username).filter(Inbox.status == EMAIL_STATUS_THRASH).paginate(
            page_number, page_size, False
        )

    @classmethod
    def fetch_sent(cls, username, page_size, page_number):
        return Email.query.join(Sent, Email.id == Sent.email).add_columns(
            Email.id, Email.created_on, Email.subject, Email.sender, Email.receivers, Email.body, Email.in_reply_to_email
        ).filter(Sent.username == username).filter(Sent.status == EMAIL_STATUS_SENT).paginate(
            page_number, page_size, False
        )

    @classmethod
    def fetch_draft(cls, username, page_size, page_number):
        return Email.query.join(Sent, Email.id == Sent.email).add_columns(
            Email.id, Email.created_on, Email.subject, Email.sender, Email.receivers, Email.body, Email.in_reply_to_email
        ).filter(Sent.username == username).filter(Sent.status == EMAIL_STATUS_DRAFT).paginate(
            page_number, page_size, False
        )

    @classmethod
    def create_email(cls, subject, sender, receivers, body, in_reply_to_email):
        from app import db
        email = Email()
        email.subject = subject
        email.sender = sender
        email.receivers = receivers
        email.body = body
        email.in_reply_to_email = in_reply_to_email
        db.session.add(email)
        db.session.commit()
        return email.id

    @classmethod
    def fetch_email_by_id(cls, email_id):
        return Email.query.filter(Email.id == email_id).first()

    @classmethod
    def update_email(cls, email_id, subject, sender, receivers, body, in_reply_to_email):
        from app import db

        email = cls.fetch_email_by_id(email_id)
        if not email:
            return cls.create_email(subject, sender, receivers, body, in_reply_to_email)

        email.subject = subject
        email.sender = sender
        email.receivers = receivers
        email.body = body
        email.in_reply_to_email = in_reply_to_email
        db.session.add(email)
        db.session.commit()
        return email.id

    @classmethod
    def create_new_draft_entry(cls, username, email_id):
        from app import db

        draft = Sent()
        draft.username = username
        draft.email = email_id
        draft.status = EMAIL_STATUS_DRAFT
        db.session.add(draft)
        db.session.commit()

    @classmethod
    def create_new_sent_entry(cls, username, email_id):
        from app import db

        # Check if it was stored as a draft. If so then update the status. Otherwise add a new entry
        draft = Sent.query.filter(Sent.email == email_id and Sent.username == username).first()
        if draft:
            draft.status = EMAIL_STATUS_SENT
            db.session.add(draft)
        else:
            sent = Sent()
            sent.username = username
            sent.email = email_id
            sent.status = EMAIL_STATUS_SENT
            db.session.add(sent)
        db.session.commit()

    @classmethod
    def create_new_inbox_entry(cls, username, email_id):
        from app import db

        inbox = Inbox()
        inbox.username = username
        inbox.email = email_id
        inbox.status = EMAIL_STATUS_INBOX
        db.session.add(inbox)
        db.session.commit()

    @classmethod
    def move_to_thrash(cls, username, email_id):
        from app import db

        inbox = Inbox.query.filter(Inbox.username == username and Inbox.email == email_id).first()
        if not inbox:
            raise InvalidRequestException()
        inbox.status = EMAIL_STATUS_THRASH
        db.session.add(inbox)
        db.session.commit()



