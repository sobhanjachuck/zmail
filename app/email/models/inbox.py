from app.db_init import db
from app.common.model_mixins import TimeStampMixin


class Inbox(TimeStampMixin):
    __tablename__ = "inbox"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), nullable=False, index=True)
    email = db.Column(db.Integer, db.ForeignKey('email.id'), nullable=False)
    status = db.Column(db.String(64), nullable=False)
    read = db.Column(db.Boolean, nullable=False, default=False)

    def __repr__(self):
        return '<Inbox - {username} :: {email}>'.format(username=self.username, email=self.email)

