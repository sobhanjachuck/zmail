from app.db_init import db
from app.common.model_mixins import TimeStampMixin


class Sent(TimeStampMixin):
    __tablename__ = "sent"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), nullable=False, index=True)
    email = db.Column(db.Integer, db.ForeignKey('email.id'), nullable=False)
    status = db.Column(db.String(64), nullable=False)

    def __repr__(self):
        return '<Sent - {username} :: {email}>'.format(username=self.username, email=self.email)