from app.common.model_mixins import TimeStampMixin
from app.db_init import db


class Email(TimeStampMixin):
    __tablename__ = "email"

    id = db.Column(db.Integer, primary_key=True)
    subject = db.Column(db.String(512), nullable=False, index=True)
    sender = db.Column(db.String(64), nullable=False, index=True)
    receivers = db.Column(db.String(1024))
    body = db.Column(db.Text, nullable=False)
    in_reply_to_email = db.Column(db.Integer, db.ForeignKey('email.id'), nullable=True)

    def __repr__(self):
        return '<User - {name} :: {username}>'.format(name=self.first_name, username=self.username)


class Attachment:
    __tablename__ = "attachement"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.Integer, db.ForeignKey('email.id'), nullable=False)
    file_path = db.Column(db.String(1024), nullable=False)


class EmailThread:
    __tablename__ = "thread"
    id = db.Column(db.Integer, primary_key=True)
    parent_email = db.Column(db.Integer, db.ForeignKey('email.id'), nullable=False)
