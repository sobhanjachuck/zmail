from app.email.email_repository import EmailRepository
from app.common.constants import EMAIL_STATUS_INBOX, EMAIL_STATUS_DRAFT, EMAIL_STATUS_SENT, EMAIL_STATUS_THRASH


class EmailService:
    @classmethod
    def fetch_emails(cls, username, box_name, page_size, page_number):
        result = list()
        if box_name.lower() == EMAIL_STATUS_INBOX:
            result = EmailRepository.fetch_inbox(username, page_size, page_number)
        elif box_name.lower() == EMAIL_STATUS_THRASH:
            result = EmailRepository.fetch_thrash(username, page_size, page_number)
        elif box_name.lower() == EMAIL_STATUS_SENT:
            result = EmailRepository.fetch_sent(username, page_size, page_number)
        elif box_name.lower() == EMAIL_STATUS_DRAFT:
            result = EmailRepository.fetch_draft(username, page_size, page_number)

        if not result or result.total == 0:
            return list()

        emails_list = list()

        # Create email_list from result
        for email in result.items:
            email_info = {
                "email_id": email.id,
                "created_on": str(email.created_on),
                "subject": email.subject,
                "sender": email.sender,
                "receivers": email.receivers.split(";") if email.receivers else [],
                "body": email.body,
                "in_reply_to_email": email.in_reply_to_email,
            }
            if box_name in (EMAIL_STATUS_INBOX, EMAIL_STATUS_THRASH):
                email_info["is_read"] = email.read

            emails_list.append(email_info)
        return emails_list

    @classmethod
    def save_as_draft(cls, username, email_dict):
        # If email_id exists, then update the email.
        # Otherwise create a new email
        # Insert a row in sent table and mark it as draft
        email_id = email_dict.get("email_id")
        receivers_list = ';'.join(email_dict["receivers"])
        email_dict["receivers"] = receivers_list
        if not email_id:
            email_id = EmailRepository.create_email(
                subject=email_dict["subject"],
                sender=email_dict["sender"],
                receivers=email_dict["receivers"],
                body=email_dict["body"],
                in_reply_to_email=email_dict.get("in_reply_to_email", None)
            )
            EmailRepository.create_new_draft_entry(username, email_id)
        else:
            EmailRepository.update_email(
                email_id=email_id,
                subject=email_dict["subject"],
                sender=email_dict["sender"],
                receivers=email_dict["receivers"],
                body=email_dict["body"],
                in_reply_to_email=email_dict.get("in_reply_to_email", None)
            )
        return email_id

    @classmethod
    def send_email(cls, username, email_dict):
        email_id = email_dict.get("email_id")
        receivers = email_dict["receivers"]
        receivers_list = ';'.join(email_dict["receivers"])
        email_dict["receivers"] = receivers_list

        # Update sent box of sender. Remove from draft if it was a draft
        if not email_id:
            email_id = EmailRepository.create_email(
                subject=email_dict["subject"],
                sender=email_dict["sender"],
                receivers=email_dict["receivers"],
                body=email_dict["body"],
                in_reply_to_email=email_dict.get("in_reply_to_email", None)
            )
            EmailRepository.create_new_sent_entry(username, email_id)
        else:
            EmailRepository.update_email(
                email_id=email_id,
                subject=email_dict["subject"],
                sender=email_dict["sender"],
                receivers=email_dict["receivers"],
                body=email_dict["body"],
                in_reply_to_email=email_dict.get("in_reply_to_email", None)
            )
            EmailRepository.create_new_sent_entry(username, email_id)

        # Update Inbox of all receivers
        for receiver in receivers:
            username = receiver[:-10]  # remove @zmail.com from email to get username
            EmailRepository.create_new_inbox_entry(username, email_id)
        return email_id

    @classmethod
    def delete_email(cls, username, email_id):
        EmailRepository.move_to_thrash(username, email_id)
