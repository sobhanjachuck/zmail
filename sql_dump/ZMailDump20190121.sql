CREATE DATABASE  IF NOT EXISTS `zmail` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `zmail`;
-- MySQL dump 10.13  Distrib 8.0.13, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: zmail
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alembic_version`
--

DROP TABLE IF EXISTS `alembic_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `alembic_version` (
  `version_num` varchar(32) NOT NULL,
  PRIMARY KEY (`version_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alembic_version`
--

LOCK TABLES `alembic_version` WRITE;
/*!40000 ALTER TABLE `alembic_version` DISABLE KEYS */;
INSERT INTO `alembic_version` VALUES ('b64146375fc1');
/*!40000 ALTER TABLE `alembic_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `email` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(512) NOT NULL,
  `sender` varchar(64) NOT NULL,
  `receivers` varchar(1024) DEFAULT NULL,
  `body` text NOT NULL,
  `in_reply_to_email` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `in_reply_to_email` (`in_reply_to_email`),
  KEY `ix_email_created_on` (`created_on`),
  KEY `ix_email_sender` (`sender`),
  KEY `ix_email_subject` (`subject`),
  CONSTRAINT `email_ibfk_1` FOREIGN KEY (`in_reply_to_email`) REFERENCES `email` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email`
--

LOCK TABLES `email` WRITE;
/*!40000 ALTER TABLE `email` DISABLE KEYS */;
INSERT INTO `email` VALUES ('2019-01-21 08:18:43','2019-01-21 08:23:50',1,'Test Email Draft','sobhanjachuck@zmail.com','souravganguly@zmail.com;yuvrajsingh@zmail.com','This is a test email body. Append testing',NULL),('2019-01-21 09:15:08','2019-01-21 09:15:08',2,'Test Email Send','sobhanjachuck@zmail.com','souravganguly@zmail.com;yuvrajsingh@zmail.com','This is a test email body. Sending to Saurav and yuvi',NULL),('2019-01-21 09:15:57','2019-01-21 09:15:57',3,'Test Email Send2','sobhanjachuck@zmail.com','souravganguly@zmail.com;yuvrajsingh@zmail.com','This is a test email2 body. Sending to Saurav and yuvi',NULL),('2019-01-21 09:18:13','2019-01-21 09:18:13',4,'Test Email Send2','sobhanjachuck@zmail.com','souravganguly@zmail.com;yuvrajsingh@zmail.com','This is a test email2 body. Sending to Saurav and yuvi',NULL),('2019-01-21 09:18:22','2019-01-21 09:18:22',5,'Test Email Send3','sobhanjachuck@zmail.com','souravganguly@zmail.com;yuvrajsingh@zmail.com','This is a test email3 body. Sending to Saurav and yuvi',NULL);
/*!40000 ALTER TABLE `email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbox`
--

DROP TABLE IF EXISTS `inbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `inbox` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `email` int(11) NOT NULL,
  `status` varchar(64) NOT NULL,
  `read` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `ix_inbox_created_on` (`created_on`),
  KEY `ix_inbox_username` (`username`),
  CONSTRAINT `inbox_ibfk_1` FOREIGN KEY (`email`) REFERENCES `email` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbox`
--

LOCK TABLES `inbox` WRITE;
/*!40000 ALTER TABLE `inbox` DISABLE KEYS */;
INSERT INTO `inbox` VALUES ('2019-01-21 09:18:13','2019-01-21 09:18:13',1,'souravganguly',4,'inbox',0),('2019-01-21 09:18:13','2019-01-21 09:33:52',2,'yuvrajsingh',4,'thrash',0),('2019-01-21 09:18:22','2019-01-21 09:18:22',3,'souravganguly',5,'inbox',0),('2019-01-21 09:18:22','2019-01-21 09:18:22',4,'yuvrajsingh',5,'inbox',0);
/*!40000 ALTER TABLE `inbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sent`
--

DROP TABLE IF EXISTS `sent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sent` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `email` int(11) NOT NULL,
  `status` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  KEY `ix_sent_created_on` (`created_on`),
  KEY `ix_sent_username` (`username`),
  CONSTRAINT `sent_ibfk_1` FOREIGN KEY (`email`) REFERENCES `email` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sent`
--

LOCK TABLES `sent` WRITE;
/*!40000 ALTER TABLE `sent` DISABLE KEYS */;
INSERT INTO `sent` VALUES ('2019-01-21 08:18:43','2019-01-21 08:18:43',1,'sobhanjachuck',1,'draft'),('2019-01-21 09:15:08','2019-01-21 09:15:08',2,'sobhanjachuck',2,'draft'),('2019-01-21 09:15:57','2019-01-21 09:15:57',3,'sobhanjachuck',3,'draft'),('2019-01-21 09:18:13','2019-01-21 09:18:13',4,'sobhanjachuck',4,'sent'),('2019-01-21 09:18:22','2019-01-21 09:18:22',5,'sobhanjachuck',5,'sent');
/*!40000 ALTER TABLE `sent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `created_on` datetime DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `last_name` varchar(64) NOT NULL,
  `alt_email` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_user_username` (`username`),
  KEY `ix_user_created_on` (`created_on`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('2019-01-20 18:52:36','2019-01-20 18:52:36',1,'sobhanjachuck','password','Sobhan','Jachuck','sj@g.com'),('2019-01-20 19:01:03','2019-01-20 19:01:03',3,'anilsahu','password','Anil','Sahu','as@g.com'),('2019-01-20 19:02:47','2019-01-20 19:02:47',4,'satishshah','password','Satish','Shah','ss@g.com'),('2019-01-20 19:03:08','2019-01-20 19:03:08',5,'sachintendulkar','password','Sachin','Tendulkar','st@g.com'),('2019-01-20 19:03:28','2019-01-20 19:03:28',6,'yuvrajsingh','password','Yuvraj','Singh','ys@g.com'),('2019-01-20 19:09:25','2019-01-20 19:09:25',7,'virendersehwag','password','Virender','Sehwag','vs@g.com'),('2019-01-21 07:42:46','2019-01-21 07:42:46',8,'souravganguly','password','Sourav','Ganguly','sg@g.com');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-21  9:37:37
