import os


class Config(object):
    DEBUG = True
    MYSQL = {
        'user': 'sobhan',
        'pw': 'password123',
        'db': 'zmail',
        'host': 'localhost',
        'port': '3306',
    }
    #SQLALCHEMY_DATABASE_URI = 'mysql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % MYSQL
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://%(user)s:%(pw)s@%(host)s/%(db)s' % MYSQL

    LOGGING_CONFIG = {
        'version': 1,
        'formatters': {
            'default': {
                'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'level': 'INFO',
                'formatter': 'default',
                'stream': 'ext://sys.stdout'
            },
            'file': {
                'class': 'logging.FileHandler',
                'level': 'INFO',
                'formatter': 'default',
                'filename': 'app.log'
            }
        },
        'loggers': {
            'console': {
                'level': 'INFO',
                'handlers': ['console'],
                'propagate': 'no'
            },
            'file': {
                'level': 'INFO',
                'handlers': ['file'],
                'propagate': 'no'
            }
        },
        'root': {
            'level': 'INFO',
            'handlers': ['console', 'file']
        }
    }

    INTEGRATION_CONFIG = {
        "OTP_BACKEND_ENDPOINT": "http://172.40.10.74:8050",
        "direct": {
            "channel": "hero",
            "defaultRatePlan": "EP",
            "urls": {
                "itinerary": "https://traffic.treebo.be/api/web/v6/checkout/itinerary/?channel={channel}&checkin={checkin}&checkout={checkout}&couponcode=&hotel_id={hotel_id}&rateplan={rate_plan}&roomconfig={room_config}&roomtype={room_type}&utm_source=direct"
            }
        }
    }

    EXTERNAL_API_TIMEOUT = 60
    SLACK_ENDPOINT = 'https://hooks.slack.com/services/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'

    GEO_CONFIG = dict(
        NEARBY_RADIUS=50.0,  # in KM
        RADIUS_OF_THE_EARTH=6373.0,  # in km
        INFINITE_DISTANCE=10000.0  # in km
    )

    CACHE_EXPIRY = 15 * 60  # 15 * 1 min
