from conf.base import Config


class LocalConfig(Config):
    ENV = 'local'
    DEBUG = True
    LOCAL_TIMEZONE = 'Asia/Kolkata'
    SESSION_COOKIE_AGE = 30
