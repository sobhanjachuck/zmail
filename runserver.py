import os
from app import app


if __name__ == '__main__':
    os.environ['FLASK_APP'] = app.name

    # Run flask application
    os.system("python3 -m flask run --host=0.0.0.0 --port=8005")
