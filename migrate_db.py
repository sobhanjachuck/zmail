"""
Migrate model changes to database

Supports only one app as of now.

TODO: Provide multiple app support
"""


import os

if __name__ == '__main__':

    from app import app
    os.environ['FLASK_APP'] = app.name

    root_dir = os.path.dirname(os.path.realpath(__file__))

    app_migration_dir = os.path.join(root_dir, app.name, 'migrations')

    if not (os.path.isdir(app_migration_dir)):

        # Initiate the db
        os.system("python -m flask db init")

    # Migration to be done
    os.system("python -m flask db migrate")

    # Implement the migrations to db
    os.system("python -m flask db upgrade")